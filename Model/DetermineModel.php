<?php

Class DetermineModel {
    
    private $inputs;
    private $outputs;
    private $routes = array();
    
    public function __construct( $contents ){
        $load = explode( ";", $contents );
        $this->inputs = explode( ",",  rtrim($load[0]) );
        $this->outputs = explode( ",", rtrim($load[1]) );
        unset( $load[0] );
        unset( $load[1] );
        foreach ($load as $l) {
            $tmp = explode(",",$l);
            if (count($tmp)>1) {
                $this->routes[] = array('from' => $tmp[0], 'to' => $tmp[1], 'state' => $tmp[2]);
            };
        } 
    }
      
    public function get($name) {
        return $this->$name;
    }
    
}

?>
