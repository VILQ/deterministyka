<?php

Class DetermineController {
    
    private $inputs;
    private $outputs;
    private $routes = array();
    private $states = array(0,1);
    public $output = array();
    
    public function __construct( $data = stdClass ){
        $this->inputs = $data->get('inputs');
        $this->outputs = $data->get('outputs');
        $this->routes = $data->get('routes');
        $this->getStartRoutes();
        $this->getRoutes();
    }
        
    private function determineRoute( $from = array(), $state = 0 ) {
        $to = array();
        foreach( $from as $start ) {
            foreach( $this->routes as $route ) {
                if ( $route['from'] == $start && $route['state'] == $state ) {
                    $to[] = $route['to'];
                }
            }
        }
        sort($to);
        return array_unique($to);
    }
    
    private function getStartRoutes() {
        foreach ( $this->states as $state ){
            $this->output[implode("",$this->inputs).$state] = $this->determineRoute( $this->inputs, $state );
        }
    }
    
    private function getRoutes() {
        $complete = false;
        while ( !$complete ){
            $complete = true;
            foreach( $this->output as $keys) {
                if ( !empty($keys) && ( !in_array( implode("",$keys).'0', array_keys($this->output)) || !in_array( implode("",$keys).'1', array_keys($this->output))) ) {
                    $complete = false;
                }
            }
            if ( $complete ) {
                break;
            }
            foreach( $this->output as $newRoute) {

                foreach ( $this->states as $state ){
                    $this->output[implode("",$newRoute).$state] = $this->determineRoute( $newRoute, $state );
                }
                unset( $this->output[0] ); // puste stany
                unset( $this->output[1] ); // puste stany
            }
        }
        $this->output['start'] = implode("",$this->inputs);
        $this->output['ends'] = $this->outputs;        
    }
    
}

?>
