<?php

Class View {
    
    private $html;
    private $header;
    private $content;
    private $footer;
    private $form = '';
    
    
    public function __construct(){
        $this->header = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>'."\n";
        $this->content = '';
        $this->footer = '   </body>
</html>';
        
        $this->view();
    }
    
    public function view() {
        $this->registerJS('jquery-1.9.1.min');
        $this->registerJS('func');
        $this->registerCSS('main');
        $this->newForm();
        $this->fillForm();
        $this->finishForm();
        $this->content = $this->form;
        echo $this->header.$this->content.$this->footer;     
    }
    
    private function registerJS( $script ){
        $tmp = explode('</head>',$this->header);
        $this->header = implode('<script src="assets/js/'.$script.'.js"></script></head>',$tmp);
    }
    
    private function registerCSS( $css ){
        $tmp = explode('</head>',$this->header);
        $this->header = implode('<link rel="stylesheet" href="assets/css/'.$css.'.css" /></head>',$tmp);
    }
    private function addHtmlInput( $name = 'name', $type= 'text', $value = '', $label = '', $class = '', $new_line = false ) {
        $this->form .= (($label)?'<label for="'.$name.'" class="'.$class.'">'.$label.'</label>':'').'<input type="'.$type.'" name="'.$name.'" id="'.$name.'" class="'.$class.'" value="'.$value.'"/>'.(($new_line)?'<br class="'.$class.'"/>':'')."\n";
    }
    
    private function newForm(){
        $this->form = '<form method="post">'."\n";     
    }
    private function fillForm(){
        $this->addHtmlInput('data','hidden');
        $this->addHtmlInput('beg', 'text', '', 'Wejścia:', 'start');
        $this->addHtmlInput('begB', 'button', 'Dodaj', '', 'start', true );
        $this->addHtmlInput('end', 'text', '', 'Końce:', 'stop');
        $this->addHtmlInput('endB', 'button', 'Dodaj', '', 'stop' );
        $this->addHtmlInput('dBegin', 'text', '', 'Stan początkowy:', 'inputData', true);
        $this->addHtmlInput('dEnd', 'text', '', 'Stan końcowy:', 'inputData', true);
        $this->addHtmlInput('dTrans', 'text', '', 'Tranzycja:', 'inputData', true);
        $this->addHtmlInput('transB', 'button', 'Dodaj', '', 'inputData', true );
        $this->addHtmlInput('','submit','Generuj');        
    }
    private function finishForm(){
        $ths->form .= '<form>'."\n";
        
    }
            
}
?>
