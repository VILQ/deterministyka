<?php

    require_once( 'Controler/DetermineControler.php' );
    require_once( 'Controler/DetermineView.php' );
    require_once( 'Model/DetermineModel.php' );
    require_once( 'View/ViewController.php' );
       
    if ( isset($_POST) && isset($_POST['data']) ){
        $routes = new DetermineController( new DetermineModel( $_POST['data'] ) ); 
        new DetermineView( serialize($routes->output) );
    }else{
        new View();
    }
?>
