<?php

Class DetermineView {
    
    private $start;
    private $ends = array();
    private $startPoints = array();
    private $cords = array();
    private $black;
    private $white;
    private $red;
    private $green;
    private $blue;
    private $_image;
    private $size;
    private $angle = 0;
    private $stateCount;
    private $fontPath = 'View/arial.ttf';
        
    public function __construct( $serializedArray = '', $size = 300 ){
        $tmp = unserialize($serializedArray);
        $this->start = $tmp['start'];
        $this->ends = $tmp['ends'];
        unset($tmp['start']);
        unset($tmp['ends']);
        $this->cords = $tmp;
        $this->stateCount = count($this->cords)/2;
        $this->angle = 26*$this->stateCount;
        $this->size = 200+15*count($this->cords);
        $this->prepareImage();
        $this->drawRoutes();
        $this->drawStates();
        $this->drawStart();
        $this->displayImage(); // z parametrem - tryb debug, eg. $this->startPoints
    }

    private function checkStateNumber($key){
        $tmp = 1;
        foreach(array_keys($this->cords) as $keys){
            if ( $key == $keys ) {
                return ceil($tmp/2);
            }
            $tmp++;
        }
    }
    
    private function prepareImage(){
        $this->_image = imagecreate(2.2*$this->size, 2*$this->size) ;
        $bg = imagecolorallocate($this->_image, 255, 255, 255);
        $this->black = imagecolorallocate($this->_image, 0, 0, 0);
        $this->white = imagecolorallocate($this->_image, 255, 255, 255);        
        $this->red = imagecolorallocate($this->_image, 255, 0, 0);
        $this->green = imagecolorallocate($this->_image, 0, 200, 0);        
        $this->blue = imagecolorallocate($this->_image, 0, 0, 255);
    }
    
    private function drawLine($a, $b, $from, $state)
    {
            $dir = ($state)?$state:-1;
            $empty = false;
            if ( !$b ) {
                $b = $a;
                $empty = true;
            }
            $r = $this->size-40;
            $angle = $a*(360/$this->stateCount)+round($this->angle);
            $x1 = $this->size+round($r*(cos(deg2rad($angle))));
            $y1 = $this->size+round($r*(sin(deg2rad($angle))));
            $angle = $b*(360/$this->stateCount)+round($this->angle);
            $x2 = $this->size+round($r*(cos(deg2rad($angle))));
            $y2 = $this->size+round($r*(sin(deg2rad($angle))));
            if ( !$empty ){       
                $half_x = (($x1+$x2)/2)-130*$state+50;
                $half_y = ($y1+$y2)/2;
                if ( $a != $b ){ //przejscie
                    imageline($this->_image, $half_x, $half_y, $x2, $y2, $this->black); 
                    imageline($this->_image, $x1, $y1, $half_x, $half_y, $this->black); 
                    $arrow = array ( 
                                        $half_x-(round($half_x-$x1-150)*0.1), $half_y-(round($half_y-$y1+10)*0.1), 
                                        $half_x, $half_y,
                                        $half_x-(round($half_x-$x1+150)*0.1), $half_y-(round($half_y-$y1)*0.1)
                    );
                    imagefilledpolygon($this->_image, $arrow, 3,$this->black); 
                    imagestring($this->_image, 5, (($x1+$x2)/2)-75*$dir, ($y1+$y2)/2-10, "$state", ($state?$this->green:$this->red) );
                } else { // petla
                    imageline($this->_image, $x1, $y1, $x1+50*$dir, $half_y+10, $this->black); 
                    imageline($this->_image, $x1, $y1, $x1+50*$dir, $half_y-10, $this->black);
                    $arrow = array ( 
                                        $x1+50*$dir-5, $half_y+5,
                                        $x1+50*$dir, $half_y-3,
                                        $x1+50*$dir+5, $half_y+5
                    );
                    imagefilledpolygon ( $this->_image, $arrow, 3, $this->black); 
                    imageline($this->_image, $x1+50*$dir, $half_y+10, $x1+50*$dir, $half_y-10, $this->black); 
                    imagestring($this->_image, 2, $x1+62*$dir, $y1-5, "$state", ($state?$this->green:$this->red) );                
                }
            } else { //zbior pusty
                imagestring($this->_image, 5, $x1+50*$dir, ($y1+$y2)/2-20, "$state", ($state?$this->green:$this->red) );
                imageline($this->_image, $x1, $y1, $x1+75*$dir, $y1, $this->black);
                imagefilledellipse($this->_image, $x1+75*$dir, $y1, 32, 32, $this->black);
                imagefilledellipse($this->_image, $x1+75*$dir, $y1, 30, 30, $this->white);
                imagettftext($this->_image, 12, 0, $x1+74*$dir-3+3*$dir, $y1+6, $this->black, $this->fontPath, chr(216));

            }
            if ($from) {
                $add = true;
                foreach ($this->startPoints as $sPoint){
                    if ( $from == $sPoint[2]) {
                        $add = false;
                    }
                }
                if ($add){
                    $this->startPoints[] = array($x1,$y1,$from);
                }
            }
    }
    
    private function drawStart(){
        $point = array();
        foreach ($this->startPoints as $state) {
            if ( $this->start == $state[2]) {
                $point = $state;
            }
        }
        $a=20;
        imageline($this->_image, $point[0]-$a, $point[1]-$a, $point[0]-50, $point[1]-50, $this->black);
        $arrow = array ( 
                    $point[0]-$a, $point[1]-$a,
                    $point[0]-$a-15, $point[1]-$a,
                    $point[0]-$a+2, $point[1]-$a-15
                );
        imagefilledpolygon ( $this->_image, $arrow, 3, $this->black); 
    }
    
    private function drawStates(){
        foreach ($this->startPoints as $point){       
            $tmp = str_split($point[2]);
            imagefilledellipse($this->_image, $point[0], $point[1], 52, 52, $this->black);
            imagefilledellipse($this->_image, $point[0], $point[1], 50, 50, $this->white);
            foreach ($tmp as $val){
                if ( in_array($val,$this->ends)){
                    imagefilledellipse($this->_image, $point[0], $point[1], 40, 40, $this->black);
                    imagefilledellipse($this->_image, $point[0], $point[1], 38, 38, $this->white);
                }
            }
            imagestring($this->_image, 10, $point[0]-4*strlen($point[2]), $point[1]-7, "$point[2]", $this->blue);
        }
    }
    
    private function drawRoutes() {
        foreach ($this->cords as $key => $val){
            $x = $this->checkStateNumber($key);
            $y = (!empty($val))?$this->checkStateNumber(implode("",$val).'0'):0;
            $this->drawLine($x,$y,substr($key,0,-1),substr($key,-1)); // from
        } 
    }
    
    private function displayImage( $debug = false ){
        if ( $debug ){
            echo "<pre>";
            print_r($debug);
            die();
        }
        header("Content-type: image/png") ;
       imagepng($this->_image);
    }
}

?>
